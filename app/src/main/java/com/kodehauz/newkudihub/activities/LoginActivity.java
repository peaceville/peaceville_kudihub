package com.kodehauz.newkudihub.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

public class LoginActivity extends AppCompatActivity {

    private TextView signUpTextView;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }

        EditText userNameEditView = (EditText) findViewById(R.id.login_username);
        EditText passwordEditView = (EditText) findViewById(R.id.login_password);

        signUpTextView = (TextView) findViewById(R.id.sign_up_here);
        loginBtn = (Button) findViewById(R.id.login_button);

        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setupIntent = new Intent(LoginActivity.this, SetUpAccountActivity.class);
                startActivity(setupIntent);
                finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(LoginActivity.this, LandingActivity.class);
                startActivity(loginIntent);;
                finish();
            }
        });
    }
}
