package com.kodehauz.newkudihub.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;

import com.kodehauz.newkudihub.R;


public class SplashActivity extends AppCompatActivity {

    private final Handler mGoToSetUpHandler = new Handler();

    private final Runnable mGoToSetUoRunnable = new Runnable() {
        @Override
        public void run() {
            goToSetup();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger go to setup shortly after the activity has been created (10 seconds)
        delayedGoToSetUpActivity(3000);
    }

    private void goToSetup() {
        Intent tutorialIntent = new Intent(SplashActivity.this, VideoTutorialActivity.class);
        startActivity(tutorialIntent);
        finish();
    }

    private void delayedGoToSetUpActivity(int delayMillis) {
        mGoToSetUpHandler.postDelayed(mGoToSetUoRunnable, delayMillis);
    }
}
