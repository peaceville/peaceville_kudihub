package com.kodehauz.newkudihub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

public class VideoTutorialActivity extends AppCompatActivity {

    private TextView skipTutorialTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_tutorial);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }

        ImageView playImage = (ImageView) findViewById(R.id.play_image_btn);
        TextView clickToWatch = (TextView) findViewById(R.id.click_watch_text_view);

        skipTutorialTextView = (TextView) findViewById(R.id.skip_tutorial_text);
        skipTutorialTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setUpIntent = new Intent(VideoTutorialActivity.this, SetUpAccountActivity.class);
                startActivity(setUpIntent);
                finish();
            }
        });
    }
}
