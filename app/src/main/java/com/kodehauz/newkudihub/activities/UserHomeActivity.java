package com.kodehauz.newkudihub.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.dialogFragments.ExtrasDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.ProfileDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.SettingsDialogFragment;
import com.kodehauz.newkudihub.fragments.AvailableOffersFragment;
import com.kodehauz.newkudihub.fragments.AvailableRequestsFragment;
import com.kodehauz.newkudihub.fragments.HomeFragment;
import com.kodehauz.newkudihub.fragments.NotificationsFragment;
import com.kodehauz.newkudihub.fragments.OffersFragment;
import com.kodehauz.newkudihub.fragments.RequestsFragment;


public class UserHomeActivity extends AppCompatActivity implements AvailableOffersFragment.OnScrollListener, AvailableRequestsFragment.OnScrollListener {

    private TextView mTextMessage;
    private Fragment currentFragment;
    public ActionBar mActionBar;
    public BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        mActionBar = getSupportActionBar();
        mActionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg));

//        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int titleId;
            String fragTag;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    titleId = R.string.title_home;
                    fragTag = "home_frag";
                    currentFragment = new HomeFragment();
                    break;
                case R.id.navigation_requests:
                    titleId = R.string.title_bids;
                    fragTag = "my_requests_frag";
                    currentFragment = new RequestsFragment();
                    break;
                case R.id.navigation_offers:
                    titleId = R.string.title_offer;
                    fragTag = "my_offers_frag";
                    currentFragment = new OffersFragment();
                    break;
                case R.id.navigation_notifications:
                    titleId = R.string.title_notifications;
                    fragTag = "notifications_frag";
                    currentFragment = new NotificationsFragment();
                    break;
                default:
                    titleId = R.string.title_home;
                    fragTag = "home_frag";
                    currentFragment = new HomeFragment();
            }
            setTitle(titleId);

            // Start a fragment transaction and replace the fragment corresponding to the menu item.
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, currentFragment, fragTag);
            fragmentTransaction.commit();

            return true;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.log_out_btn:
                Toast.makeText(this, "User logged out!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.profile_id:
                showProfileDialog();
                break;
            case R.id.settings_id:
                showSettingseDialog();
                break;
            case R.id.extras_id:
                showExtrasDialog();
                break;
        }
        return true;
    }


    private void showProfileDialog() {
        ProfileDialogFragment profileDialogFragment = ProfileDialogFragment.newInstance("Profile");
        profileDialogFragment.setCancelable(false);
        profileDialogFragment.show(this.getSupportFragmentManager(), "dialog_fragment_profile");
    }

    private void showSettingseDialog() {
        SettingsDialogFragment settingsDialogFragment = SettingsDialogFragment.newInstance("Settings");
        settingsDialogFragment.setCancelable(false);
        settingsDialogFragment.show(this.getFragmentManager(), "dialog_fragment_settings");
    }

    private void showExtrasDialog() {
        ExtrasDialogFragment extrasDialogFragment = ExtrasDialogFragment.newInstance("Extras");
        extrasDialogFragment.setCancelable(false);
        extrasDialogFragment.show(getFragmentManager(), "dialog_fragment_extras");
    }

    private void showDialogFragment(String fragTitle, String fragTag) {

    }

    @Override
    public void scrollHappened(int dx, int dy) {
        HomeFragment fragmentHome = (HomeFragment) getSupportFragmentManager().findFragmentByTag("home_frag");
        fragmentHome.hideFabOnScroll(dx, dy);
    }


}
