package com.kodehauz.newkudihub.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

public class SetUpAccountActivity extends AppCompatActivity {

    private Button signUpBtn;
    private TextView loginTextView;
    private EditText usernameEditView;
    private EditText emailEditView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_account);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }

        signUpBtn = (Button) findViewById(R.id.sign_up_btn);
        loginTextView = (TextView) findViewById(R.id.login_click_text_view);
        usernameEditView = (EditText) findViewById(R.id.username_edit_view);
        emailEditView = (EditText) findViewById(R.id.email_address_edit_view);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent finishSetupIntent = new Intent(SetUpAccountActivity.this, FinishAccountSetUpActivity.class);
                startActivity(finishSetupIntent);;
                finish();
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(SetUpAccountActivity.this, LoginActivity.class);
                startActivity(loginIntent);;
                finish();
            }
        });
    }
}
