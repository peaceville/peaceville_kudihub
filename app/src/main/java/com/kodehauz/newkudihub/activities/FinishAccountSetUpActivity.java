package com.kodehauz.newkudihub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

public class FinishAccountSetUpActivity extends AppCompatActivity {

    private Button finishSetupBtn;
    private TextView loginTextView;
    private EditText verificationEditText;
    private CheckBox termsAgreementCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_account_set_up);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.hide();
        }

        verificationEditText = (EditText) findViewById(R.id.verification_code_view);
        termsAgreementCheckBox = (CheckBox) findViewById(R.id.agreement_check_box);

        finishSetupBtn = (Button) findViewById(R.id.sign_up_btn);
        loginTextView = (TextView) findViewById(R.id.login_click_text_view);

        finishSetupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent finishSetupIntent = new Intent(FinishAccountSetUpActivity.this, LoginActivity.class);
                startActivity(finishSetupIntent);;
                finish();
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(FinishAccountSetUpActivity.this, LoginActivity.class);
                startActivity(loginIntent);;
                finish();
            }
        });
    }
}
