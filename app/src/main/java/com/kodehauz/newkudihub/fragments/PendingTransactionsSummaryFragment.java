package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;


public class PendingTransactionsSummaryFragment extends Fragment {

    Button viewTransactionsBtn;
    TextView pendingTransactionsCountView;

    public PendingTransactionsSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pending_transactions_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        viewTransactionsBtn = (Button) view.findViewById(R.id.view_transactions_btn);
        pendingTransactionsCountView = (TextView) view.findViewById(R.id.pending_transactions_count_text);
    }

}
