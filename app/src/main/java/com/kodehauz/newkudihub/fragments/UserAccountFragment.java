package com.kodehauz.newkudihub.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

import org.w3c.dom.Text;


public class UserAccountFragment extends Fragment {

    private RatingBar ratingBar;
    private ImageView profileImageView;
    private TextView userFullnameView;
    private Button changeImageBtn;
    private EditText phoneEditView;
    private EditText emailEditView;
    private EditText oldPassEditView;
    private EditText newPassEditView;

    public UserAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ratingBar =(RatingBar) view.findViewById(R.id.rating_star_value);
        profileImageView = (ImageView) view.findViewById(R.id.user_pic_view);
        userFullnameView = (TextView) view.findViewById(R.id.profile_full_name);
        changeImageBtn = (Button) view.findViewById(R.id.change_image_btn);
        phoneEditView = (EditText) view.findViewById(R.id.phone_number_field);
        emailEditView = (EditText) view.findViewById(R.id.email_edit_field);
        oldPassEditView = (EditText) view.findViewById(R.id.old_password_edit_view);
        newPassEditView = (EditText) view.findViewById(R.id.new_password_edit_view);
    }

}
