package com.kodehauz.newkudihub.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.kodehauz.newkudihub.R;


public class NewBankAccountChildFragment extends Fragment {

    private Button cancelBtn;
    private Button addBtn;
    private Spinner bankSpinner;
    private EditText acctNameEditView;
    private EditText acctNumberEditView;

    public NewBankAccountChildFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_bank_account_child, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        cancelBtn = (Button) view.findViewById(R.id.new_acct_cancel_btn);
        addBtn = (Button) view.findViewById(R.id.new_acct_add_btn);
        bankSpinner = (Spinner) view.findViewById(R.id.bank_spinner_view);
        acctNameEditView = (EditText) view.findViewById(R.id.account_name_edit_view);
        acctNumberEditView = (EditText) view.findViewById(R.id.account_number_edit_view);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFragment();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFragment();
            }
        });
    }

    // listens to the interface
    private void hideFragment() {
        // communicate parent activity through the implemented listener
        BankDetailsFragListener listener = (BankDetailsFragListener) getParentFragment();
        listener.hideNewAccountFragment();
    }

    public interface BankDetailsFragListener {
        void hideNewAccountFragment();
    }

}
