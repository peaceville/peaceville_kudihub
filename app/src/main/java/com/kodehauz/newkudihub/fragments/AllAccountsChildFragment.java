package com.kodehauz.newkudihub.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.BankAccount;
import com.kodehauz.newkudihub.recyclerAdapters.BankAccountRecyclerAdapter;

import java.util.ArrayList;

public class AllAccountsChildFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager layoutManager;
    private BankAccountRecyclerAdapter accountsAdapter;

    public AllAccountsChildFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_accounts_child, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.all_accounts_list);
        populateUi(BankAccount.loadTestBankAccounts(getContext()));
    }

    public void populateUi(ArrayList<BankAccount> accounts) {
        accountsAdapter = new BankAccountRecyclerAdapter(accounts, getContext());

        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(accountsAdapter);
    }


}
