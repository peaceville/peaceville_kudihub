package com.kodehauz.newkudihub.fragments;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.activities.UserHomeActivity;
import com.kodehauz.newkudihub.dialogFragments.NewDealDialog;
import com.kodehauz.newkudihub.dialogFragments.NewOrEditOfferDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.NewOrEditRequestDialogFragment;
import com.kodehauz.newkudihub.models.Request;
import com.kodehauz.newkudihub.recyclerAdapters.RequestRecyclerAdapter;
import com.kodehauz.newkudihub.viewPagerAdapters.AvailableRequestsOffersFragmentPagerAdapter;
import com.kodehauz.newkudihub.viewPagerAdapters.HomeFragmentsPagerAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private ViewPager mItemsViewPager;
    private RequestRecyclerAdapter requestsAdapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView mRecyclerView;
    private ViewPager availableOffersRequestsViewPager;
    private int currentPage;
    private boolean isFabOpen = false;

    private Fragment topPagersFragment;

    Timer timer;
    final long DELAY_MS = 1000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 5000; // time in milliseconds between successive task executions.
    private Animation fab_open;
    private Animation fab_close;
    private Animation rotate_forward;
    private Animation rotate_backward;
    private FloatingActionButton fab;
    private TextView fab_request;
    private TextView fab_offer;
    private RelativeLayout viewsContainer;

    UserHomeActivity parentActivity;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        parentActivity = (UserHomeActivity) getActivity();

        mItemsViewPager = (ViewPager) view.findViewById(R.id.top_pager);
        availableOffersRequestsViewPager = (ViewPager) view.findViewById(R.id.available_offer_bids_viewpager);

        mItemsViewPager.setAdapter(new HomeFragmentsPagerAdapter(getActivity().getSupportFragmentManager(), getActivity()));
        availableOffersRequestsViewPager.setAdapter(new AvailableRequestsOffersFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity()));

        fab = (FloatingActionButton) view.findViewById(R.id.new_deal_button);
        fab_offer = (TextView)view.findViewById(R.id.fab_offer);
        fab_request = (TextView) view.findViewById(R.id.fab_request);

        fab_open = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.rotate_backward);

        viewsContainer = (RelativeLayout) view.findViewById(R.id.all_views_container);

        /*After setting the adapter use the timer to automate swiping */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 4 - 1) {
                    currentPage = 0;
                }
                mItemsViewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabDots);
        TabLayout bottomTabLayout = (TabLayout) view.findViewById(R.id.view_pager_titles);

        tabLayout.setupWithViewPager(mItemsViewPager, true);
        bottomTabLayout.setupWithViewPager(availableOffersRequestsViewPager, true);

        // to prevent the fragment from snapping to the bottom of the page when launched, we set the focus on the scrollview
        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.container_scroll_view);
        if (scrollView != null) {
            scrollView.setFocusableInTouchMode(true);
            scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

            // hide floating action button while scrolling
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    int scrollY = scrollView.getScrollY(); // For ScrollView

                    if (scrollY > 0 && fab.getVisibility() == View.VISIBLE) {
                        fab.hide();
                    } else if (scrollY < 0 && fab.getVisibility() != View.VISIBLE) {
                        fab.show();
                    }
                }
            });
        }

        fab.setOnClickListener(this);
        fab_offer.setOnClickListener(this);
        fab_request.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void populateUi(ArrayList<Request> requests) {
        Log.d("PRODUCTS_OBTAINED", requests.toString());
        requestsAdapter = new RequestRecyclerAdapter(requests, getActivity());

        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(requestsAdapter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (id){
            case R.id.new_deal_button:
                animateFAB();
                break;
            case R.id.fab_offer:
                animateFAB();

                Fragment offerFrag = new OffersFragment();
                fragmentTransaction.replace(R.id.fragment_container, offerFrag, "my_offers_frag");
                fragmentTransaction.commit();
                parentActivity.setTitle("My Offers");
                parentActivity.navigation.setSelectedItemId(R.id.navigation_offers);
                break;
            case R.id.fab_request:
                animateFAB();
                Fragment requestFrag = new RequestsFragment();
                fragmentTransaction.replace(R.id.fragment_container, requestFrag, "my_requests_frag");
                fragmentTransaction.commit();
                parentActivity.setTitle("My Requests");
                parentActivity.navigation.setSelectedItemId(R.id.navigation_requests);
                break;
        }
    }

    public void animateFAB(){

        if(isFabOpen){
            fab.startAnimation(rotate_backward);
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
            viewsContainer.setAlpha(1);     // blurr the background
            enableDisableView(viewsContainer, true);
            fab_offer.startAnimation(fab_close);
            fab_request.startAnimation(fab_close);
            fab_offer.setClickable(false);
            fab_request.setClickable(false);
            isFabOpen = false;

        } else {
            fab.startAnimation(rotate_forward);
            fab.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            viewsContainer.setAlpha(0.1f);  // blurr the background
            enableDisableView(viewsContainer, false);
            fab_offer.startAnimation(fab_open);
            fab_request.startAnimation(fab_open);
            fab_offer.setClickable(true);
            fab_request.setClickable(true);
            isFabOpen = true;

        }
    }

    public void hideFabOnScroll(int dx, int dy) {
        if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
            fab.hide();
        } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
            fab.show();
        }
    }

    // method for disabling/enabling click event in the blurred background layout
    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }
}
