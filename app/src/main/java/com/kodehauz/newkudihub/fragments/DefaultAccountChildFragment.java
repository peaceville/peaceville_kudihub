package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.BankAccount;
import com.kodehauz.newkudihub.recyclerAdapters.BankAccountRecyclerAdapter;


public class DefaultAccountChildFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private BankAccountRecyclerAdapter accountsAdapter;
    private LinearLayoutManager layoutManager;
    private TextView summaryAcctNameTextView;
    private TextView summaryAcctNumTextView;
    private TextView summaryBankTextView;
    private EditText acctNameEditView;
    private EditText acctNumEditView;
    private Spinner bankSpinnerView;
    private CheckBox preferredCheckBox;
    private View defaultView;
    private View detailsView;
    private ConstraintLayout layout;
    private boolean isExpanded;

    public DefaultAccountChildFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_default_account_child, container, false);

//        mRecyclerView = (RecyclerView) view.findViewById(R.id.default_account_list);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        summaryAcctNameTextView = (TextView) view.findViewById(R.id.acct_name_text_view);
        summaryAcctNumTextView = (TextView) view.findViewById(R.id.acct_num_text_view);
        summaryBankTextView = (TextView) view.findViewById(R.id.bank_text_view);

        acctNameEditView = (EditText) view.findViewById(R.id.acct_name_edit_view);
        acctNumEditView = (EditText) view.findViewById(R.id.acct_num_edit_view);
        bankSpinnerView = (Spinner) view.findViewById(R.id.bank_spinner_view);
        preferredCheckBox = (CheckBox) view.findViewById(R.id.preferred_check_box);

        defaultView = (View) view.findViewById(R.id.default_view);
        detailsView = (View) view.findViewById(R.id.detailed_view);

        layout = (ConstraintLayout) view.findViewById(R.id.whole_acct_view);

        populateUi(BankAccount.loadTestDefaultAccount(getContext()));
    }

    public void populateUi(BankAccount account) {
        summaryAcctNameTextView.setText(account.getAccountName().substring(0, 6).concat("..."));
        summaryAcctNumTextView.setText(account.getAccountNumber().substring(0, 6).concat("..."));
        summaryBankTextView.setText(account.getBankName().substring(0, 6).concat("..."));

        acctNameEditView.setText(account.getAccountName());
        acctNumEditView.setText(account.getAccountNumber());
        bankSpinnerView.setSelection(1);
        preferredCheckBox.setChecked(account.isPreferred());

        defaultView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
        detailsView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isExpanded = !isExpanded;

                if (isExpanded) {
                    layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 420));
                }
                else {
                    layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 120));
                }
                defaultView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
                detailsView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            }
        });
    }


}
