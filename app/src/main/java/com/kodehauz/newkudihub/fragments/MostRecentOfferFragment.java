package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;


public class MostRecentOfferFragment extends Fragment {

    private TextView offerValueView;
    private TextView offerRateView;

    public MostRecentOfferFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_most_recent_offer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        offerValueView = (TextView) view.findViewById(R.id.offer_value);
        offerRateView = (TextView) view.findViewById(R.id.offer_rate);
    }


}
