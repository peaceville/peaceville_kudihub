package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kodehauz.newkudihub.R;


public class BankDetailsFragment extends Fragment implements NewBankAccountChildFragment.BankDetailsFragListener {

    public Button addNewAccountBtn;
    private Fragment defaultAccountFragment;
    private Fragment allAccountsFragment;
    private Fragment newAccountFragment;

    public BankDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bank_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        addNewAccountBtn = view.findViewById(R.id.add_new_account_btn);

        defaultAccountFragment = new DefaultAccountChildFragment();
        allAccountsFragment = new AllAccountsChildFragment();
        newAccountFragment = new NewBankAccountChildFragment();

        addNewAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewAccountFragment();
            }
        });
        insertNestedFragments();
    }


    // Embeds the child fragment dynamically
    private void insertNestedFragments() {
        if (defaultAccountFragment == null && allAccountsFragment == null) {
            defaultAccountFragment = new DefaultAccountChildFragment();
            allAccountsFragment = new AllAccountsChildFragment();
        }
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.default_account_child_fragment_container, defaultAccountFragment);
        transaction.replace(R.id.all_accounts_child_fragment_container, allAccountsFragment);
        transaction.commit();
    }

    private void showNewAccountFragment() {
        if (newAccountFragment == null) {
            newAccountFragment = new NewBankAccountChildFragment();
        }
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.enter_new_account_details_child_fragment_container, newAccountFragment, "new_account_fragment").commit();

        // hide the add button
        addNewAccountBtn.setVisibility(View.GONE);
    }

    @Override
    public void hideNewAccountFragment() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        Fragment newAccountFrag = getChildFragmentManager().findFragmentByTag("new_account_fragment");
        transaction.remove(newAccountFrag).commit();

        // show the add button
        addNewAccountBtn.setVisibility(View.VISIBLE);
    }



}
