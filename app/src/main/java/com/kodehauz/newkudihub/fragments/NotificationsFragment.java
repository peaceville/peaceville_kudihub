package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.Notification;
import com.kodehauz.newkudihub.recyclerAdapters.NotificationRecyclerAdapter;

import java.util.ArrayList;


public class NotificationsFragment extends Fragment {

    private LinearLayoutManager layoutManager;
    private RecyclerView mRecyclerView;
    private NotificationRecyclerAdapter notificationAdapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.notifications_list);

        populateUi(Notification.loadTestNotifications());
    }

    public void populateUi(ArrayList<Notification> notices) {
        notificationAdapter = new NotificationRecyclerAdapter(notices, getActivity());

        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(notificationAdapter);
    }

}
