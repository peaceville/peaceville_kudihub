package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.dialogFragments.NewOrEditOfferDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.TransactionProgressDialogFragment;
import com.kodehauz.newkudihub.models.Request;
import com.kodehauz.newkudihub.recyclerAdapters.MyDealRecyclerAdapter;

import java.util.ArrayList;


public class OffersFragment extends Fragment {

    private MyDealRecyclerAdapter dealsAdapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView mRecyclerView;
    private FloatingActionButton fab;

    public OffersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_offers, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.requests_list);

        populateUi(Request.loadTestRequests());

        fab = (FloatingActionButton) view.findViewById(R.id.add_deal_button);

        // to prevent the fragment from snapping to the bottom of the page when launched, we set the focus on the scrollview
        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.container_scroll_view);
        if (scrollView != null) {
            scrollView.setFocusableInTouchMode(true);
            scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

            // hide floating action button while scrolling
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    int scrollY = scrollView.getScrollY(); // For ScrollView

                    if (scrollY > 0 && fab.getVisibility() == View.VISIBLE) {
                        fab.hide();
                    } else if (scrollY < 0 && fab.getVisibility() != View.VISIBLE) {
                        fab.show();
                    }
                }
            });
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewOfferDialog();
            }
        });
    }

    public void populateUi(ArrayList<Request> requests) {
        dealsAdapter = new MyDealRecyclerAdapter(requests, getActivity());

        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(dealsAdapter);
    }

    private void showNewOfferDialog() {
        NewOrEditOfferDialogFragment newOfferDialogFragment = NewOrEditOfferDialogFragment.newInstance("New Offer");
        newOfferDialogFragment.setCancelable(false);
        newOfferDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_new_offer");
    }

}
