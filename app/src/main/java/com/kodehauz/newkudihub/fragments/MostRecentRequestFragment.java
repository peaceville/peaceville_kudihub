package com.kodehauz.newkudihub.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;


public class MostRecentRequestFragment extends Fragment {

    private TextView requestValueView;
    private TextView requestRateView;

    public MostRecentRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_most_recent_request, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        requestValueView = (TextView) view.findViewById(R.id.request_value);
        requestRateView = (TextView) view.findViewById(R.id.request_rate);
    }


}
