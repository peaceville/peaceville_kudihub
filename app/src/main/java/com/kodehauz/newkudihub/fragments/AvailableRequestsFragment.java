package com.kodehauz.newkudihub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.Request;
import com.kodehauz.newkudihub.recyclerAdapters.RequestRecyclerAdapter;
import com.kodehauz.newkudihub.viewPagerAdapters.AvailableRequestsOffersFragmentPagerAdapter;

import java.util.ArrayList;


public class AvailableRequestsFragment extends Fragment {

    private RequestRecyclerAdapter requestsAdapter;
    private LinearLayoutManager layoutManager;
    public RecyclerView mRecyclerView;
    private OnScrollListener onScrollListener;
    private String tag;
    private static Fragment thisFragment;

    public AvailableRequestsFragment() {
        // Required empty public constructor
    }

    public static AvailableRequestsFragment newInstance(String tag){
        AvailableRequestsFragment fragmentClass = new AvailableRequestsFragment();
        Bundle bundle = new Bundle();
        bundle.putString ("tag",tag);
        fragmentClass.setArguments(bundle);
        thisFragment = fragmentClass;
        return fragmentClass;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = getArguments().getString("tag");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_requests_available, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.available_requests_list);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                OnScrollListener listener = (OnScrollListener) getActivity();
                listener.scrollHappened(dx, dy);

            }
        });

        populateUi(Request.loadTestRequests());
    }

    public void populateUi(ArrayList<Request> requests) {
        requestsAdapter = new RequestRecyclerAdapter(requests, getActivity());

        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(requestsAdapter);
    }

    public interface OnScrollListener {
        void scrollHappened(int dx, int dy);
    }
}
