package com.kodehauz.newkudihub.recyclerAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.dialogFragments.CompleteTransactionDetailDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.MatchedDetailDialogFragment;
import com.kodehauz.newkudihub.dialogFragments.RequestDetailsDialogFragment;
import com.kodehauz.newkudihub.models.Request;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class MyDealRecyclerAdapter extends RecyclerView.Adapter<MyDealRecyclerAdapter.ViewHolder> {

    private ArrayList<Request> mRequests;
    // Store the context for easy access
    private Activity mContext;

    public MyDealRecyclerAdapter(ArrayList<Request> requests, Activity context) {
        this.mRequests = requests;
        this.mContext = context;
    }

    @Override
    public MyDealRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View productView = inflater.inflate(R.layout.my_deal_item, parent, false);

        return new ViewHolder(productView);
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Request thisRequest = mRequests.get(position);

        TextView timeView = viewHolder.timeTextView;
        TextView valueView = viewHolder.valueTextView;
        TextView rateView = viewHolder.rateTextView;
        TextView statusView = viewHolder.statusTextView;
        ImageView picView = viewHolder.ownerPicView;

        timeView.setText(position + " mins ago");
        valueView.setText(thisRequest.getValue());
        rateView.setText(thisRequest.getRate());
        statusView.setText(thisRequest.getStatus());
        switch (thisRequest.getStatus()) {
            case "Waiting":
                statusView.setTextColor(Color.GRAY);
                break;
            case "Matched":
                statusView.setTextColor(Color.YELLOW);
                break;
            case "Completed":
                statusView.setTextColor(Color.GREEN);
                break;
        }

        viewHolder.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                switch (thisRequest.getStatus()) {
                    case "Waiting":
                        RequestDetailsDialogFragment requestDetailsDialogFragment = RequestDetailsDialogFragment.newInstance("Request Details");
                        requestDetailsDialogFragment.setCancelable(false);
                        requestDetailsDialogFragment.show(mContext.getFragmentManager(), "dialog_fragment_request_details");
                        break;
                    case "Matched":
                        MatchedDetailDialogFragment matchedTransactionDialogFragment = MatchedDetailDialogFragment.newInstance("Details");
                        matchedTransactionDialogFragment.setCancelable(false);
                        matchedTransactionDialogFragment.show(mContext.getFragmentManager(), "dialog_fragment_matched_details");
                        break;
                    case "Completed":
                        CompleteTransactionDetailDialogFragment completeTransactionDialogFragment = CompleteTransactionDetailDialogFragment.newInstance("Details");
                        completeTransactionDialogFragment.setCancelable(false);
                        completeTransactionDialogFragment.show(mContext.getFragmentManager(), "dialog_fragment_complete_transaction");
                        break;
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView timeTextView;
        public TextView statusTextView;
        public TextView valueTextView;
        public TextView rateTextView;
        public ImageView ownerPicView;

        // Define listener member variable
        private OnItemClickListener listener;

        public ViewHolder(View itemView) {
            super(itemView);

            ownerPicView = (ImageView) itemView.findViewById(R.id.deal_owner_pic_view);
            timeTextView = (TextView) itemView.findViewById(R.id.deal_time);
            statusTextView = (TextView) itemView.findViewById(R.id.deal_status);
            valueTextView = (TextView) itemView.findViewById(R.id.deal_value);
            rateTextView = (TextView) itemView.findViewById(R.id.deal_rate);

            // Attach a click listener to the entire row view
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                listener.onItemClick(itemView, position);
            }
        }

        // Define the method that allows the parent activity or fragment to define the listener
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }

    @Override
    public int getItemCount() {
        return mRequests.size();
    }

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }



}
