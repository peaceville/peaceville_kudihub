package com.kodehauz.newkudihub.recyclerAdapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.BankAccount;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class BankAccountRecyclerAdapter extends RecyclerView.Adapter<BankAccountRecyclerAdapter.ViewHolder> {

    private ArrayList<BankAccount> mAccounts;
    // Store the context for easy access
    private Context mContext;

    private int mExpandedPosition = -1;
    private int previousExpandedPosition = -1;

    public BankAccountRecyclerAdapter(ArrayList<BankAccount> accounts, Context context) {
        this.mAccounts = accounts;
        this.mContext = context;
    }

    @Override
    public BankAccountRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.bank_account_item, parent, false);

        return new ViewHolder(view);
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        BankAccount thisAccount = mAccounts.get(position);

        TextView nameTextView = viewHolder.summaryAcctNameTextView;
        TextView numberTextView = viewHolder.summaryAcctNumTextView;
        TextView bankTextView = viewHolder.summaryBankTextView;

        EditText nameEditView = viewHolder.acctNameEditView;
        EditText numberEditView = viewHolder.acctNumEditView;
        Spinner bankSpinner = viewHolder.bankSpinnerView;
        CheckBox preferredCheck = viewHolder.preferredCheckBox;

//        nameTextView.setText(thisAccount.getAccountName());
        nameTextView.setText(thisAccount.getAccountName().substring(0, 6).concat("..."));
        numberTextView.setText(thisAccount.getAccountNumber().substring(0, 6).concat("..."));
        bankTextView.setText(thisAccount.getBankName().substring(0, 6).concat("..."));

        nameEditView.setText(thisAccount.getAccountName());
        numberEditView.setText(thisAccount.getAccountNumber());
        bankSpinner.setSelection(position);
        preferredCheck.setChecked(thisAccount.isPreferred());

        final boolean isExpanded = position==mExpandedPosition;
        viewHolder.defaultView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
        viewHolder.detailsView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        viewHolder.itemView.setActivated(isExpanded);

        if (isExpanded)
            previousExpandedPosition = position;

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView summaryAcctNameTextView;
        public TextView summaryAcctNumTextView;
        public TextView summaryBankTextView;

        public EditText acctNameEditView;
        public EditText acctNumEditView;
        public Spinner bankSpinnerView;
        public CheckBox preferredCheckBox;

        public View detailsView;
        public View defaultView;

        public ViewHolder(View itemView) {
            super(itemView);

            summaryAcctNameTextView = (TextView) itemView.findViewById(R.id.acct_name_text_view);
            summaryAcctNumTextView = (TextView) itemView.findViewById(R.id.acct_num_text_view);
            summaryBankTextView = (TextView) itemView.findViewById(R.id.bank_text_view);

            acctNameEditView = (EditText) itemView.findViewById(R.id.acct_name_edit_view);
            acctNumEditView = (EditText) itemView.findViewById(R.id.acct_num_edit_view);
            bankSpinnerView = (Spinner) itemView.findViewById(R.id.bank_spinner_view);
            preferredCheckBox = (CheckBox) itemView.findViewById(R.id.preferred_check_box);

            defaultView = (View) itemView.findViewById(R.id.default_view);
            detailsView = (View) itemView.findViewById(R.id.detailed_view);

            // Attach a click listener to the entire row view
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                listener.onItemClick(itemView, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAccounts.size();
    }

    /***** Creating OnItemClickListener *****/

    // Define listener member variable
    private OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
