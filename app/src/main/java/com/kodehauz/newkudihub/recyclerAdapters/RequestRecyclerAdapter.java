package com.kodehauz.newkudihub.recyclerAdapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.Request;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class RequestRecyclerAdapter extends RecyclerView.Adapter<RequestRecyclerAdapter.ViewHolder> {

    private ArrayList<Request> mRequests;
    // Store the context for easy access
    private Context mContext;

    public RequestRecyclerAdapter(ArrayList<Request> requests, Context context) {
        this.mRequests = requests;
        this.mContext = context;
    }

    @Override
    public RequestRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View productView = inflater.inflate(R.layout.request_item, parent, false);

        return new ViewHolder(productView);
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Request thisRequest = mRequests.get(position);

        TextView ownerView = viewHolder.ownerTextView;
        TextView valueView = viewHolder.valueTextView;
        TextView rateView = viewHolder.rateTextView;

        ownerView.setText(thisRequest.getOwner());
        valueView.setText(thisRequest.getValue());
        rateView.setText(thisRequest.getRate());

        viewHolder.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                switch (thisRequest.getStatus()) {
                    case "Waiting":

                        break;
                    case "Matched":

                        break;
                    case "Completed":

                        break;
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView ownerTextView;
        public TextView valueTextView;
        public TextView rateTextView;

        // Define listener member variable
        private OnItemClickListener listener;

        public ViewHolder(View itemView) {
            super(itemView);

            ownerTextView = (TextView) itemView.findViewById(R.id.request_owner);
            valueTextView = (TextView) itemView.findViewById(R.id.request_value);
            rateTextView = (TextView) itemView.findViewById(R.id.request_rate);

            // Attach a click listener to the entire row view
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                listener.onItemClick(itemView, position);
            }
        }

        // Define the method that allows the parent activity or fragment to define the listener
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }

    @Override
    public int getItemCount() {
        return mRequests.size();
    }

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }



}
