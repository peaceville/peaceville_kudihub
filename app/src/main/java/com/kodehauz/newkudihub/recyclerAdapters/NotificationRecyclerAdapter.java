package com.kodehauz.newkudihub.recyclerAdapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.dialogFragments.MatchedDetailDialogFragment;
import com.kodehauz.newkudihub.models.Notification;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder> {

    private ArrayList<Notification> mNotices;
    // Store the context for easy access
    private Activity mContext;

    public NotificationRecyclerAdapter(ArrayList<Notification> notices, Activity context) {
        this.mNotices = notices;
        this.mContext = context;
    }

    @Override
    public NotificationRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.notification_item, parent, false);

        return new ViewHolder(view);
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Notification thisNotice = mNotices.get(position);

        TextView msgView = viewHolder.msgBodyTextView;
        TextView timeView = viewHolder.timeTextView;

        msgView.setText(thisNotice.getMessage());
        timeView.setText(thisNotice.getTime());

        viewHolder.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                MatchedDetailDialogFragment matchedTransactionDialogFragment = MatchedDetailDialogFragment.newInstance("Details");
                matchedTransactionDialogFragment.setCancelable(false);
                matchedTransactionDialogFragment.show(mContext.getFragmentManager(), "dialog_fragment_matched_details");
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView msgBodyTextView;
        public TextView timeTextView;

        // Define listener member variable
        private OnItemClickListener listener;

        public ViewHolder(View itemView) {
            super(itemView);

            msgBodyTextView = (TextView) itemView.findViewById(R.id.notification_body_text_view);
            timeTextView = (TextView) itemView.findViewById(R.id.notification_time);

            // Attach a click listener to the entire row view
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                listener.onItemClick(itemView, position);
            }
        }

        // Define the method that allows the parent activity or fragment to define the listener
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }

    @Override
    public int getItemCount() {
        return mNotices.size();
    }

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

}
