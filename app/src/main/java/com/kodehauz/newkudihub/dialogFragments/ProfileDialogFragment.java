package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.viewPagerAdapters.ProfileDialogFragmentPagerAdapter;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class ProfileDialogFragment extends DialogFragment {

    private EditText requestAmountEditView;
    private EditText requestRateEditView;
    private EditText requestBankSelectView;
    private Button saveBtn;
    private Button cancelBtn;
    private ProgressDialog pd;
    private TextView titleTextView;
    private ImageView titleBarCloseBtn;
    private ViewPager profileViewPager;

    public ProfileDialogFragment() {
    }

    public static ProfileDialogFragment newInstance(String title) {
        ProfileDialogFragment frag = new ProfileDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_profile, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cancelBtn = (Button) view.findViewById(R.id.profile_cancel_button);
        saveBtn = (Button) view.findViewById(R.id.profile_send_button);
        profileViewPager = (ViewPager) view.findViewById(R.id.profile_dialog_viewpager);
        profileViewPager.setAdapter(new ProfileDialogFragmentPagerAdapter(getChildFragmentManager(), getActivity()));
        final TabLayout tabLayout = (TabLayout) view.findViewById(R.id.profile_view_pager_titles);

        tabLayout.setupWithViewPager(profileViewPager, true);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Profile");
        titleTextView.setText(title);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

//    @Override
//    protected float getDownScaleFactor() {
//        // Allow to customize the down scale factor.
//        return 1.5f;
//    }
//
//    @Override
//    protected int getBlurRadius() {
//        // Allow to customize the blur radius factor.
//        return 3;
//    }
//
//    @Override
//    protected boolean isActionBarBlurred() {
//        // Enable or disable the blur effect on the action bar.
//        // Disabled by default.
//        return true;
//    }
//
//    @Override
//    protected boolean isDimmingEnable() {
//        // Enable or disable the dimming effect.
//        // Disabled by default.
//        return true;
//    }
//
//    @Override
//    protected boolean isRenderScriptEnable() {
//        // Enable or disable the use of RenderScript for blurring effect
//        // Disabled by default.
//        return true;
//    }
//
//    @Override
//    protected boolean isDebugEnable() {
//        // Enable or disable debug mode.
//        // False by default.
//        return false;
//    }
//
    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        super.onResume();
    }
}
