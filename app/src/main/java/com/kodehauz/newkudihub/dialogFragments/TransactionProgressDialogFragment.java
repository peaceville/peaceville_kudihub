package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class TransactionProgressDialogFragment extends BlurDialogFragment {

    private Button completeTrasnactionBtn;
    private Button uploadPaymentProofBtn;
    private ProgressDialog pd;
    private TextView titleTextView;
    private TextView amountTextView;
    private TextView bankNameTextView;
    private TextView acctNameTextView;
    private TextView acctNumberTextView;
    private TextView payerNameTextView;
    private Button paymentConfirmationBtn;

    public TransactionProgressDialogFragment() {
    }

    public static TransactionProgressDialogFragment newInstance(String title) {
        TransactionProgressDialogFragment frag = new TransactionProgressDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_transaction_progress, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        completeTrasnactionBtn = (Button) view.findViewById(R.id.complete_transaction_btn);
        uploadPaymentProofBtn = (Button) view.findViewById(R.id.upload_payment_proof_btn);
        paymentConfirmationBtn = (Button) view.findViewById(R.id.payment_confirmation_btn);

        amountTextView = (TextView) view.findViewById(R.id.amount_text_view);
        bankNameTextView = (TextView) view.findViewById(R.id.bank_name_text_view);
        acctNameTextView = (TextView) view.findViewById(R.id.acct_name_text_view);
        acctNumberTextView = (TextView) view.findViewById(R.id.acct_number_text_view);
        payerNameTextView = (TextView) view.findViewById(R.id.payer_name_text_view);

        pd = new ProgressDialog(getActivity());

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Transaction Progress");
        titleTextView.setText(title);


        // Show soft keyboard automatically and request focus to field
//        requestAmountEditView.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        uploadPaymentProofBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogUploadProof(getActivity());
            }
        });

        completeTrasnactionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransactionCompletedSuccessfullyDialogFragment transactionCompleteSuccessDialogFragment = TransactionCompletedSuccessfullyDialogFragment.newInstance("Transaction Completed Successfully");
                transactionCompleteSuccessDialogFragment.setCancelable(false);
                transactionCompleteSuccessDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_transaction_complete_success");
                dismiss();
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        super.onResume();
    }
}
