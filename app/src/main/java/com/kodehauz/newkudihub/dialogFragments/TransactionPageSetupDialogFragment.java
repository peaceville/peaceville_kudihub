package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.BankAccount;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class TransactionPageSetupDialogFragment extends BlurDialogFragment {

    private Button submitBtn;
    private Button cancelBtn;
    private ProgressDialog pd;
    private TextView titleTextView;
    private ImageView titleBarCloseBtn;
    private TextView summaryAcctNameTextView;
    private TextView summaryAcctNumTextView;
    private TextView summaryBankTextView;
    private EditText acctNameEditView;
    private EditText acctNumEditView;
    private Spinner bankSpinnerView;
    private CheckBox preferredCheckBox;
    private View defaultView;
    private View detailsView;
    private ConstraintLayout layout;
    private Spinner anotherBankSpinner;
    private EditText anotherAcctNameEditView;
    private EditText anotherAcctNumberEditView;
    private RadioButton defaultAcctRadioBtn;
    private RadioButton anotherAcctRadioBtn;

    private boolean isExpanded;

    public TransactionPageSetupDialogFragment() {
    }

    public static TransactionPageSetupDialogFragment newInstance(String title) {
        TransactionPageSetupDialogFragment frag = new TransactionPageSetupDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_setup_transaction_page, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        submitBtn = (Button) view.findViewById(R.id.submit_btn);
        cancelBtn = (Button) view.findViewById(R.id.cancel_btn);

        summaryAcctNameTextView = (TextView) view.findViewById(R.id.acct_name_text_view);
        summaryAcctNumTextView = (TextView) view.findViewById(R.id.acct_num_text_view);
        summaryBankTextView = (TextView) view.findViewById(R.id.bank_text_view);

        acctNameEditView = (EditText) view.findViewById(R.id.acct_name_edit_view);
        acctNumEditView = (EditText) view.findViewById(R.id.acct_num_edit_view);
        bankSpinnerView = (Spinner) view.findViewById(R.id.bank_spinner_view);
        preferredCheckBox = (CheckBox) view.findViewById(R.id.preferred_check_box);

        defaultView = (View) view.findViewById(R.id.default_view);
        detailsView = (View) view.findViewById(R.id.detailed_view);

        layout = (ConstraintLayout) view.findViewById(R.id.whole_acct_view);

        anotherAcctNameEditView = (EditText) view.findViewById(R.id.another_account_name_edit_view);
        anotherAcctNumberEditView = (EditText) view.findViewById(R.id.another_account_number_edit_view);
        anotherBankSpinner = (Spinner) view.findViewById(R.id.another_bank_spinner_view);
        defaultAcctRadioBtn = (RadioButton) view.findViewById(R.id.default_acct_radio_btn);
        // set the default button checked
        defaultAcctRadioBtn.setChecked(true);
        anotherAcctRadioBtn = (RadioButton) view.findViewById(R.id.another_acct_radio_btn);

        populateUi(BankAccount.loadTestDefaultAccount(getActivity()));

        pd = new ProgressDialog(getActivity());

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Transaction Page");
        titleTextView.setText(title);


        // Show soft keyboard automatically and request focus to field
//        requestAmountEditView.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransactionDetailsDialogFragment transactionDetailsDialogFragment = TransactionDetailsDialogFragment.newInstance("Transaction Details");
                transactionDetailsDialogFragment.setCancelable(false);
                transactionDetailsDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_transaction_details");
                dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        defaultAcctRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (anotherAcctRadioBtn.isChecked()) {
                    anotherAcctRadioBtn.setChecked(false);
                }
            }
        });

        anotherAcctRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (defaultAcctRadioBtn.isChecked()) {
                    defaultAcctRadioBtn.setChecked(false);
                }
            }
        });

    }

    public void populateUi(BankAccount account) {

        summaryAcctNameTextView.setText(account.getAccountName().substring(0, 6).concat("..."));
        summaryAcctNumTextView.setText(account.getAccountNumber().substring(0, 6).concat("..."));
        summaryBankTextView.setText(account.getBankName().substring(0, 6).concat("..."));

        acctNameEditView.setText(account.getAccountName());
        acctNumEditView.setText(account.getAccountNumber());
        bankSpinnerView.setSelection(1);
        preferredCheckBox.setChecked(account.isPreferred());

        defaultView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
        detailsView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isExpanded = !isExpanded;

                if (isExpanded) {
                    layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 420));

                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);

                    getDialog().getWindow().setLayout(width, height);
                }
                else {
                    layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 120));

                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.75);

                    getDialog().getWindow().setLayout(width, height);
                }
                defaultView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
                detailsView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.75);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        super.onResume();
    }
}
