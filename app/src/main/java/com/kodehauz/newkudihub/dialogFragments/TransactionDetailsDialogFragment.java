package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class TransactionDetailsDialogFragment extends BlurDialogFragment {

    private Button submitBtn;
    private Button cancelBtn;
    private ProgressDialog pd;
    private TextView titleTextView;
    private TextView transactionIdTextView, amountTextView, rateTextView, nairaEquivalentTextView, transactionFeeTextView;
    private TextView paymentSumTextView, bankNameTextView, acctNameTextView, acctNumberTextView;

    public TransactionDetailsDialogFragment() {
    }

    public static TransactionDetailsDialogFragment newInstance(String title) {
        TransactionDetailsDialogFragment frag = new TransactionDetailsDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_transaction_details, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        submitBtn = (Button) view.findViewById(R.id.next_btn);
        cancelBtn = (Button) view.findViewById(R.id.back_btn);

        transactionIdTextView = (TextView) view.findViewById(R.id.transaction_id_text_view);
        amountTextView = (TextView) view.findViewById(R.id.amount_text_view);
        rateTextView = (TextView) view.findViewById(R.id.rate_text_view);
        nairaEquivalentTextView = (TextView) view.findViewById(R.id.naira_equivalent_text_view);
        transactionFeeTextView = (TextView) view.findViewById(R.id.transaction_fee_text_view);
        paymentSumTextView = (TextView) view.findViewById(R.id.payment_sum_text_view);
        bankNameTextView = (TextView) view.findViewById(R.id.bank_name_text_view);
        acctNameTextView = (TextView) view.findViewById(R.id.acct_name_text_view);
        acctNumberTextView = (TextView) view.findViewById(R.id.acct_number_text_view);

        pd = new ProgressDialog(getActivity());

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "New Request");
        titleTextView.setText(title);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransactionProgressDialogFragment transactionProgressDialogFragment = TransactionProgressDialogFragment.newInstance("Transaction Progress");
                transactionProgressDialogFragment.setCancelable(false);
                transactionProgressDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_transaction_progress");
                dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.85);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.78);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        super.onResume();
    }
}
