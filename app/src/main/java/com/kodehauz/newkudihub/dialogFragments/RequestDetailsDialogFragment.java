package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class RequestDetailsDialogFragment extends BlurDialogFragment {


    private ProgressDialog pd;
    private TextView titleTextView;
    private Button deleteBtn;
    private TextView amountTextView;
    private TextView bankNameTextView;
    private TextView rateTextView;
    private TextView statusTextView;
    private Button editBtn;

    public RequestDetailsDialogFragment() {
    }

    public static RequestDetailsDialogFragment newInstance(String title) {
        RequestDetailsDialogFragment frag = new RequestDetailsDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_deal_details, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        amountTextView = (TextView) view.findViewById(R.id.amount_text_view);
        bankNameTextView = (TextView) view.findViewById(R.id.bank_text_view);
        rateTextView = (TextView) view.findViewById(R.id.preferred_rate_text_view);
        statusTextView = (TextView) view.findViewById(R.id.status_text_view);
        deleteBtn = (Button) view.findViewById(R.id.deal_delete_btn);
        editBtn = (Button) view.findViewById(R.id.deal_edit_btn);


        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Request Details");
        titleTextView.setText(title);

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewOrEditRequestDialogFragment editRequestDialogFragment = NewOrEditRequestDialogFragment.newInstance("Edit Request");
                editRequestDialogFragment.setCancelable(false);
                editRequestDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_edit_request");

                dismiss();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.85);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.75);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        super.onResume();
    }
}
