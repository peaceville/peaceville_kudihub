package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class MatchedDetailDialogFragment extends BlurDialogFragment {

    private ProgressDialog pd;
    private TextView titleTextView;
    private Button startTransactionBtn;
    private ImageView sellerPicView, buyerPicView;
    private RatingBar sellerRatingBar, buyerRatingBar;
    private TextView sellerNameView, buyerNameView;
    private TextView amountTextView, rateTextView;

    public MatchedDetailDialogFragment() {
    }

    public static MatchedDetailDialogFragment newInstance(String title) {
        MatchedDetailDialogFragment frag = new MatchedDetailDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_matched_detail, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        startTransactionBtn = (Button) view.findViewById(R.id.start_transaction_btn);
        sellerPicView = (ImageView) view.findViewById(R.id.seller_pic_view);
        buyerPicView = (ImageView) view.findViewById(R.id.buyer_pic_view);
        sellerRatingBar = (RatingBar) view.findViewById(R.id.seller_rating_bar);
        buyerRatingBar = (RatingBar) view.findViewById(R.id.buyer_rating_bar);
        sellerNameView = (TextView) view.findViewById(R.id.seller_name);
        buyerNameView = (TextView) view.findViewById(R.id.buyer_name);
        amountTextView = (TextView) view.findViewById(R.id.amount_view);
        rateTextView = (TextView) view.findViewById(R.id.rate_view);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Details");
        titleTextView.setText(title);

        startTransactionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransactionPageSetupDialogFragment transactionPageDialogFragment = TransactionPageSetupDialogFragment.newInstance("Transaction Page");
                transactionPageDialogFragment.setCancelable(false);
                transactionPageDialogFragment.show(getActivity().getFragmentManager(), "dialog_fragment_transaction_page");
                dismiss();
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.85);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.78);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        super.onResume();
    }
}
