package com.kodehauz.newkudihub.dialogFragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kodehauz.newkudihub.R;
import com.kodehauz.newkudihub.models.Request;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;


/**
 * Created by Ufere Peace on 07/05/2018.
 */

public class NewOrEditRequestDialogFragment extends BlurDialogFragment {

    private EditText requestAmountEditView;
    private EditText requestRateEditView;
    private Spinner requestBankSelectView;
    private Button saveRequestBtn;
    private Button cancelBtn;
    private ProgressDialog pd;
    private TextView titleTextView;

    public NewOrEditRequestDialogFragment() {
    }

    public static NewOrEditRequestDialogFragment newInstance(String title) {
        NewOrEditRequestDialogFragment frag = new NewOrEditRequestDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_fragment_new_or_edit_deal, container);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);

        requestAmountEditView = (EditText) view.findViewById(R.id.amount_edit_view);
        requestRateEditView = (EditText) view.findViewById(R.id.preferred_rate_edit_view);
        requestBankSelectView = (Spinner) view.findViewById(R.id.bank_spinner_view);
        saveRequestBtn = (Button) view.findViewById(R.id.deal_save_btn);
        cancelBtn = (Button) view.findViewById(R.id.deal_cancel_btn);
        pd = new ProgressDialog(getActivity());

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "New Request");
        titleTextView.setText(title);


        // Show soft keyboard automatically and request focus to field
        requestAmountEditView.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        saveRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String requestAmount = requestAmountEditView.getText().toString();
                String requestRate = requestRateEditView.getText().toString();
                String bankName = requestBankSelectView.getSelectedItem().toString();

//                if (!TextUtils.isEmpty(requestAmount) && !TextUtils.isEmpty(requestRate) && !TextUtils.isEmpty(bankName)) {
//                    Request newRequest = new Request();
//                    newRequest.setValue(requestAmount);
//                    newRequest.setRate(requestRate);
//                    newRequest.setOwner(bankName);
//                    //show the progress dialog
//                    pd.setTitle("Saving Request...");
//                    pd.setMessage("Please wait while Request is saved...");
//                    pd.setCanceledOnTouchOutside(false);
//                    pd.show();
////                    saveBenefiaryOperation(newRequest, accessToken);  // start register operation on a new thread
//                } else {
//                    Toast.makeText(getActivity(), "Please enter complete bank details", Toast.LENGTH_LONG).show();
//                }
                Toast.makeText(getActivity(), "Request has been sent", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected float getDownScaleFactor() {
        // Allow to customize the down scale factor.
        return 1.5f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 3;
    }

    @Override
    protected boolean isActionBarBlurred() {
        // Enable or disable the blur effect on the action bar.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        // Enable or disable debug mode.
        // False by default.
        return false;
    }

    @Override
    public void onResume() {
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.85);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.70);

        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        super.onResume();
    }
}
