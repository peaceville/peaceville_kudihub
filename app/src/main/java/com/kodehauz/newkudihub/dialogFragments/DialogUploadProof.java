package com.kodehauz.newkudihub.dialogFragments;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodehauz.newkudihub.R;


/**
 * Created by Ufere Peace on 14/05/2018.
 */

public class DialogUploadProof implements View.OnClickListener{

    public DialogUploadProof(Context context) {
        final Dialog dialog = new Dialog(context, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_upload_payment_proof);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

        LinearLayout userCamera = (LinearLayout) dialog.findViewById(R.id.use_camera_option);
        LinearLayout useFileManaager = (LinearLayout) dialog.findViewById(R.id.use_file_manager_option);
        Button closeBtn = (Button) dialog.findViewById(R.id.close_btn);
        userCamera.setOnClickListener(this);
        useFileManaager.setOnClickListener(this);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.use_camera_option :
                Log.d("Test ", " USE CAMERA ");
                break;
            case R.id.use_file_manager_option :
                Log.d("Test ", " USE FILE MANAGER ");
                break;
        }
    }
}
