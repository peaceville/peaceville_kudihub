package com.kodehauz.newkudihub.viewPagerAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kodehauz.newkudihub.fragments.PendingTransactionsSummaryFragment;
import com.kodehauz.newkudihub.fragments.MostRecentOfferFragment;
import com.kodehauz.newkudihub.fragments.MostRecentRequestFragment;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class HomeFragmentsPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Tab1", "Tab2", "Tab3" };
    private Context context;

    public HomeFragmentsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment currentFragment;

        switch (position) {
            case 0:
                currentFragment = new MostRecentRequestFragment();
                break;
            case 1:
                currentFragment = new MostRecentOfferFragment();
                break;
            case 2:
                currentFragment = new PendingTransactionsSummaryFragment();
                break;
            default:
                currentFragment = new MostRecentRequestFragment();
        }
//        return PageFragment.newInstance(position + 1);
        return currentFragment;
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        // Generate title based on item position
//        return tabTitles[position];
//    }
}
