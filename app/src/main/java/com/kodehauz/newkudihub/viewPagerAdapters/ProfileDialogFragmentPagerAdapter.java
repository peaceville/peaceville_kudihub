package com.kodehauz.newkudihub.viewPagerAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kodehauz.newkudihub.fragments.BankDetailsFragment;
import com.kodehauz.newkudihub.fragments.UserAccountFragment;


/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class ProfileDialogFragmentPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "User Account", "Bank Details" };
    private Context context;

    public ProfileDialogFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new UserAccountFragment();
            case 1:
                return new BankDetailsFragment();
            default:
                return new UserAccountFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
