package com.kodehauz.newkudihub.viewPagerAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.View;

import com.kodehauz.newkudihub.fragments.AvailableOffersFragment;
import com.kodehauz.newkudihub.fragments.AvailableRequestsFragment;

import java.lang.ref.Reference;
import java.util.ArrayList;


/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class AvailableRequestsOffersFragmentPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Available Offers", "Available Requests" };
    private Context context;
    SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public AvailableRequestsOffersFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
//        return AvailableOffersRequestsPageFragment.newInstance(position + 1);
        switch (position) {
            case 0:
                Fragment offerFragment = AvailableOffersFragment.newInstance("available_offers_frag");
//                registeredFragments.put(position, offerFragment);
                return offerFragment;
            case 1:
                Fragment requestFragment = AvailableRequestsFragment.newInstance("available_requests_frag");
//                registeredFragments.put(position, requestFragment);
                return requestFragment;
            default:
                Fragment fragment = AvailableOffersFragment.newInstance("available_offers_frag");
//                registeredFragments.put(position, fragment);
                return fragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

//    @Override
//    public void destroyItem(View container, int position, Object object) {
//        registeredFragments.remove(position);
//        super.destroyItem(container, position, object);
//    }
//
//    public Fragment getRegisteredFragment(int position) {
//        return registeredFragments.get(position);
//    }
}
