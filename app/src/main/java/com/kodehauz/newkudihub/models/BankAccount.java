package com.kodehauz.newkudihub.models;

import android.content.Context;

import com.kodehauz.newkudihub.R;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Created by Ufere Peace on 10/05/2018.
 */

public class BankAccount {

    private String accountName;
    private String accountNumber;
    private String bankName;
    private boolean preferred;

    public BankAccount(String accountName, String accountNumber, String bankName, boolean preferred) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.bankName = bankName;
        this.preferred = preferred;
    }

    public BankAccount() {
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public boolean isPreferred() {
        return preferred;
    }

    public void setPreferred(boolean preferred) {
        this.preferred = preferred;
    }

    public static ArrayList<BankAccount> loadTestBankAccounts(Context ctx) {
        ArrayList<BankAccount> accounts = new ArrayList<>();
        String[] mBanksArray = ctx.getResources().getStringArray(R.array.bank_spinner);

        for (int i = 0; i < 5; i++) {
            BankAccount acct = new BankAccount("Account-" + i, "00756575585" + i, mBanksArray[i], i == 1);
            accounts.add(acct);
        }

        return accounts;
    }

    public static BankAccount loadTestDefaultAccount(Context ctx) {
        return new BankAccount("Account-" + 1, "00756575585" + 1, ctx.getResources().getStringArray(R.array.bank_spinner)[1], true);
    }
}
