package com.kodehauz.newkudihub.models;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 05/05/2018.
 */

public class Request {

    private String owner;
    private String value;
    private String rate;
    private String status;

    public Request() {
    }

    public Request(String owner, String value, String rate, String status) {
        this.owner = owner;
        this.value = value;
        this.rate = rate;
        this.status = status;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static ArrayList<Request> loadTestRequests() {
        ArrayList<Request> requests = new ArrayList<>();
        String[] statusList = new String[] {"Waiting", "Matched", "Completed"};

        for (int i = 0; i < 10; i++) {
            Request mReq = new Request("Owner-" + i, "$10,500.0" + i, "N345.50/$", statusList[i % 3]);
            requests.add(mReq);
        }

        return requests;
    }
}
