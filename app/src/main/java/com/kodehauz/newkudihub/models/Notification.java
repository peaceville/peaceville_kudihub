package com.kodehauz.newkudihub.models;

import java.util.ArrayList;

/**
 * Created by Ufere Peace on 09/05/2018.
 */

public class Notification {

    private String title;
    private String message;
    private String time;

    public Notification() {
    }

    public Notification(String title, String message, String time) {
        this.title = title;
        this.message = message;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public static ArrayList<Notification> loadTestNotifications() {
        ArrayList<Notification> notices = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Notification notice = new Notification("Notice-" + i, "This is message-" + i + ". This is a sample notification", "2" + i + " mins ago");
            notices.add(notice);
        }

        return notices;
    }
}
